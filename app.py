from flask import Flask, jsonify, abort, request, url_for, redirect
from routes.notes import notes_routes

app = Flask(__name__)

app.url_map.strict_slashes = False

app.register_blueprint(notes_routes, url_prefix='/notes')
