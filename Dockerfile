FROM python:alpine
WORKDIR /app
RUN pip install pipenv && pipenv sync && pipenv shell
ENV FLASK_APP=app.py
ENV FLASK_DEBUG=1
ENV FLASK_ENV=development
CMD ["flask", "run", "--host", "0.0.0.0"]