from flask import Blueprint, jsonify, abort, request, url_for
from mocks.notes import notes

notes_routes = Blueprint('notes_routes', __name__)

@notes_routes.route('/', methods=['GET'])
def get_all_notes():
    return jsonify(notes)

@notes_routes.route('/<int:id>', methods=['GET'])
def get_note_by_id(id):
    # TODO: REFACTOR THIS SHIT
    note = [note for note in notes if note['id'] == id]
    if len(note) == 0:
        abort(404)
    return jsonify(note[0])

@notes_routes.route('/', methods=['POST'])
def create_note():
    if not request.json or not 'title' in request.json:
        abort(400)
    note = {
        'id': notes[-1]['id'] + 1,
        'title': request.json['title'],
        'description': request.json.get('description', ""),
        'done': False
    }
    notes.append(note)
    return jsonify(note), 201

@notes_routes.route('/<int:id>', methods=['PUT'])
def update_note(id):
    note = [note for note in notes if note['id'] == id]
    if len(note) == 0:
        abort(404)
    if not request.json:
        abort(400)
    if 'title' in request.json and type(request.json['title']) is not str:
        abort(400)
    if 'description' in request.json and type(request.json['description']) is not str:
        abort(400)
    if 'done' in request.json and type(request.json['done']) is not bool:
        abort(400)
    note[0]['title'] = request.json.get('title', note[0]['title'])
    note[0]['description'] = request.json.get('description', note[0]['description'])
    note[0]['done'] = request.json.get('done', note[0]['done'])
    return jsonify(note[0])

@notes_routes.route('/notes/<int:id>', methods=['DELETE'])
def delete_note(id):
    note = [note for note in notes if note['id'] == id]
    if len(note) == 0:
        abort(404)
    notes.remove(note[0])
    return jsonify({'result': True})